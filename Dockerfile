

FROM openjdk:7-jdk-alpine

WORKDIR /app

COPY target/*.jar app.jar

COPY run.sh .
RUN chmod a+x ./ run.sh

EXPOSE 8080

CMD ["run.sh"]








